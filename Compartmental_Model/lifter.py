import numpy as np

def create_GH_extender(inputs, outputs, diffusion_maps, copy = True):
    """
    Create a function that performs geometric harmonics for the provided data.
    """
    inputs = np.array(inputs, copy = copy)
    outputs = np.array(outputs, copy = copy)
    vals = diffusion_maps.eigenvalues
    vecs = diffusion_maps.eigenvectors
    ck = np.dot(vecs, outputs)
    alpha = ck/vals

    def GH_extender(x_new):
        d2 = np.sum((x_new[:,None,:] - inputs[None,:,:])**2.0, axis = 2)
        K = np.exp(-d2/(2.0*diffusion_maps.epsilon))
        return np.dot(K, np.dot(vecs.T, alpha))

    return GH_extender

class Lifter(object):
    def __init__(self, inputs, outputs, diffusion_maps, copy = True):
        """
        The first axis of inputs/outputs must index the points, i.e.,
              inputs.shape == (num_points, dim_inputs)
             outputs.shape == (num_points, dim_outputs)
        """
        self.X = np.array(inputs, copy = copy)
        self.Y = np.array(outputs, copy = copy)
        self.dmaps = diffusion_maps
        self.extenders = list()
        for i in range(self.Y.shape[1]):
            extender = create_GH_extender(self.X, self.Y[:,i], self.dmaps)
            self.extenders.append(extender)
    
    def __call__(self, x_new):
        """
        The first axis of x_new must index the points, i.e.,
              x_new.shape == (num_new_points, dim_inputs)
        """
        if x_new.shape[1] != self.X.shape[1]:
            raise ValueError(
                "Dimension mismatch: %s, %s" % (x_new.shape, self.X.shape)
            )
        
        output = np.zeros((x_new.shape[0], self.Y.shape[1]))
        for j in range(self.Y.shape[1]):
            output[:,j] = self.extenders[j](x_new)
        
        return output
    
    def jacobian(self, x_new):
        """
        The first axis of x_new must index the points, i.e.,
              x_new.shape == (num_new_points, dim_inputs)
        
        Returns an array, JAC, of shape
              (num_new_points, dim_outputs, dim_inputs)
        so that JAC[i,:,:] is the Jacobian of the GH function
        at the i-th point of x_new.
        """
        eps = self.dmaps.epsilon
        vals = np.reshape(self.dmaps.eigenvalues, (-1, 1))
        vecs = self.dmaps.eigenvectors
        alpha = np.matmul(vecs, self.Y)/vals
        
        diffs = (self.X[:,None,:] - x_new[None,:,:]).T
        d2 = np.sum(
            (x_new[:,None,:] - self.dmaps.points[None,:,:])**2.0,
            axis = 2
        )
        K = np.exp(-d2/(2.0*eps))
        
        JAC = np.zeros((x_new.shape[0], self.Y.shape[1], self.X.shape[1]))
        for i in range(JAC.shape[0]):
            modes = np.matmul(diffs[:,i,:]*K[i,:], vecs.T)
            JAC[i,:,:] = np.matmul(modes, alpha).T/eps
        
        return JAC