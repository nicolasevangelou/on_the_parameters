# On The Parameters

This repository contains codes and materials pertaining to the paper ``On the Parameter Combinations That Matter and on Those That do Not: Data-Driven Studies of Parameter (Non)identifiability,'' which has been accepted for publication in [PNAS Nexus](https://academic.oup.com/pnasnexus). A preprint is available [on arXiv](https://arxiv.org/abs/2110.06717).

## License

This code is released under the [MIT License](https://opensource.org/licenses/MIT). See the `LICENSE` file for additional details.

The files named `DiffusionMaps.py` and `parsimonious.py` were written by Thomas Thiem. Code for the `diffusion_maps` package (including `subsample.py`, which also exists on its own) was written by Juan M. Bello-rivas *et al.* and is available at [the project's GitHub repository](https://github.com/jmbr/diffusion-maps). It is present at multiple locations in subdirectories named `dm`. These works are also provided under the MIT License.
