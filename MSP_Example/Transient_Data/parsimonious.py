# -*- coding: utf-8 -*-
"""
Created on Fri Jul 14 15:12:45 2017

@author: tthiem

Module for implementing diffusion map dimensionality reduction tasks.
"""

from tqdm import tqdm_notebook as tqdm
import numpy as np
import scipy as sp
from scipy.spatial.distance import pdist, squareform
from scipy.sparse.linalg import eigsh
import matplotlib.pyplot as plt
from functools import partial

class DiffusionMap:
    """
    Class for running diffusion maps on data.
    
    Attributes
    ==========
    
    data : (np.ndarray)
        The data to be analyzed through diffusion maps.
        Should take the form of (nPoints, mDimensions).
    
    similarityMatrix : (np.ndarray)
        Optional, the similarity matrix corresponding to the data. Should take 
        the form of (nPoints, nPoints). If a similarity matrix is not provided,
        one will be computed using the Euclidean metric.
    
    alpha: (float)
        A value characterizing the type of diffusion used for the diffusion map.
        Typical values are: 0, 0.5 or 1.
    
    eigenvectors : (np.ndarray)
        The eigenvectors returned by the diffusion map calculation.
        Shape (nPoints, k).
    
    eigenvalues : (np.ndarray)
        The eigenvalues returned by the diffusion map calculation.
        Shape (k,).
    
    eps : (float)
        Optional, the distance used in the kernel of the diffusion map. If an
        eps is not provided, one will be computed using the median of the pairwise
        distances.
    
    diffusionMatrix : (np.ndarray)
        The diffusion matrix for which the eigenvectors and eigenvalues are calculated.
        Shape (npoints, npoints).
    
    k : (int) - The number of eigenvalues and eigenvectors to return from
        the diffusion map.
    
    residuals : (dict)
        Contains the eps scale, bandwidth type and residuals resulting from the
        local linear regression calculation.
        
        keys :  'Residuals' : the residuals
                'Eps Scale' : a scaling for the bandwidth
                'Bandwidth Type' : the closeness metric used
                'Significant Threshold' : the value above which residuals are considered important
                'Significant Indices' : the indices of the eigenvectors that are deemed important by the significance threshold

    significantIndices : (np.ndarray)
        The indices of the eigenvectors deemed important by the residual calculation.
        Found under the 'Significant Indices' key of the residuals dict.
    
    residuals_for_current_data : (boolean)
        Whether residuals have been computed for the current set of eigenvectors.
    """
    def __init__(self, data, similarityMatrix=None, alpha=1, eps=None, k=10):
        
        self.data = data
        if similarityMatrix is None:
            self.similarityMatrix = squareform(pdist(data))
        else:
            self.similarityMatrix = similarityMatrix
        self.k = k
        self.alpha = alpha
        self.eigenvalues, self.eigenvectors, self.eps, self.diffusionMatrix = DiffusionMap.diffusion_map(
                self.similarityMatrix, alpha=self.alpha, eps=eps, k=self.k)
        self.residuals_for_current_data = False
    
    @property
    def status(self):
        """
        Return the current status of the DiffusionMap object. Displays the parameters
        of the diffusion map along with the number of eigenvectors and eigenvalues
        computed as well as the status of the residuals.
        """
        print('Number of eigenvalues/eigenvectors: {0}'.format(self.k))
        print('Alpha: {0}'.format(self.alpha))
        print('Diffusion epsilon: {0}'.format(self.eps))
        print('Residuals for current data: {0}'.format(self.residuals_for_current_data))
    
    @property
    def residuals(self):
        """
        Calculate, set, and return default residuals if there are no residuals,
        else return the current residuals dict.
        
        _residuals : (dict)
            A dictionary of the residuals and the properties used to calculate them.
            
            keys :  'Residuals' : the residuals
                    'Eps Scale' : a scaling for the bandwidth
                    'Bandwidth Type' : the closeness metric used
                    'Significant Threshold' : the value above which residuals are considered important
                    'Significant Indices' : the indices of the eigenvectors that are deemed important by the significance threshold
        """
        if getattr(self, '_residuals', None) is None:
            self.setResiduals()

        return self._residuals

    @property
    def significantIndices(self):
        """
        Returns the signficant indices of the residuals dict.
        Calculates and sets default significant indices if there are no
        significant indices in the current residuals dict.
        Uses default residuals if there are no residuals.
        """
        if getattr(self, '_residuals', None) is None:
            self.setResiduals()
            self.setSignificantIndices()
        elif 'Significant Indices' not in self._residuals:
            self.setSignificantIndices()
        
        return self._residuals['Significant Indices']

    @property
    def significantEigenvectors(self):
        """
        Returns the eigenvectors corresponding to the significant indices.
        Calls the significantIndices property to ensure that there are signficiant indcies.
        """
        return self.eigenvectors[:, self.significantIndices]

    def setResiduals(self, eps_scale=3, bandwidth_type='median'):
        """
        Calculates and sets the residual dict without significant indices.
        """
        self._residuals = DiffusionMap.compute_residuals(self.eigenvectors, eps_scale=eps_scale, bandwidth_type=bandwidth_type)
        self.residuals_for_current_data = True
        
    def setSignificantIndices(self, threshold=0.3, eps_scale=3, bandwidth_type='median'):
        """
        Calculates and sets the signficant indices for the current residuals dict.
        Uses the the current residuals dict if one exists, else calls setResiduals
        to generate a residuals dict.
        """
        if getattr(self, '_residuals', None) is None:
            self.setResiduals(eps_scale=eps_scale, bandwidth_type=bandwidth_type)
        self._residuals['Significance Threshold'] = threshold
        self._residuals['Significant Indices'] = np.ravel([i for i, v in enumerate(self._residuals['Residuals']) if v > threshold])

    def getResidualsPerEps(self, eps_scale_range=[0.1, 5, 10], bandwidth_type='median'):
        """
        Calculate and return a dict of residuals per local linear regression eps value based on the current eigenvectors.
        Plots the residual values for each eps value versus the eigenvector index.
        Dict form - eps value in eps_scale_range list : residuals for eps value in the eps_scale_range list.
        """
        return DiffusionMap.residualsPerEps(self.eigenvectors, eps_scale_range=eps_scale_range, bandwidth_type=bandwidth_type)
    
    def LLRplot(self, colormap='viridis'):
        """
        Parameters
        ==========
        
        colormap : (string)
            The colormap used to color the plot by the residual values.
        """
        fig, ax = plt.subplots()    
        colors, cmap = DiffusionMap.scalarsToColormap(self.residuals['Residuals'], colormap=colormap)
        indices = np.arange(1, len(self.eigenvalues))
        ax.bar(indices, self.eigenvalues[1:], color=colors[1:], log=True)
        ax.set_xlabel('$i$')
        ax.set_ylabel('$\lambda_i$')
        cb = plt.colorbar(cmap, ax=ax, orientation='vertical')
        cb.set_label('residuals')
        plt.tight_layout()
        
        return (fig, ax)

    def gridPlot(self, colormap='viridis'):
        """
        Parameters
        ==========
        
        colormap : (string)
            The colormap used to color the plot by the residual values.
        """
        colors, cmap = DiffusionMap.scalarsToColormap(self.residuals['Residuals'])
        gridWidth = self.k - 1
        fig, axes = plt.subplots(nrows=gridWidth, ncols=gridWidth)
        for i in range(gridWidth):
            for j in range(gridWidth):
                if self.residuals['Residuals'][i+1] < self.residuals['Residuals'][j+1]:
                    color = colors[i+1]
                else:
                    color = colors[j+1]
                    
                ax = axes[j][i]
                ax.scatter(self.eigenvectors[:, i+1], self.eigenvectors[:, j+1], s=4, color=color)
                lanr = lambda i: '$\phi_{%d}$' % i
                if j == gridWidth-1:
                    ax.set_xlabel(lanr(i+1))
                ax.set_xticks([])
                if i == 0:
                    ax.set_ylabel(lanr(j+1))
                ax.set_yticks([])
                ax.grid(False)
        fig.subplots_adjust(wspace=0, hspace=0, top=.92)
        cb = plt.colorbar(cmap, ax=axes, orientation='vertical')
        cb.set_label('lesser of two residuals')
        
        return (fig, axes)

    def minmaxPlot(self, **kwargs):
        """
        Creates a plot of the data used for the diffusion map for each significant eigenvector.
        In each plot, the data point mapped to the minimum and maximum value of the
        corresponding eigenvector is highlighted.
        """
        fig, axes = plt.subplots(nrows=len(self.significantIndices), **kwargs)
        
        if len(self.significantIndices) == 1:
            ax = axes
            for i, v in enumerate(self.significantEigenvectors.T):
                ax.plot(self.data.T, color='black', alpha=0.1, linestyle='-')
                ax.plot(self.data[np.argmin(v), :], color='blue', linestyle='-', lw=4, label='min')
                ax.plot(self.data[np.argmax(v), :], color='red', linestyle='-', lw=4, label='max')
                ax.legend(loc='best')
                ax.set_title('Trajectories at extremes of $\phi_%d$' % self.significantIndices[i])
        else:
            for i, v in enumerate(self.significantEigenvectors.T):
                ax = axes[i]
                ax.plot(self.data.T, color='black', alpha=0.1, linestyle='-')
                ax.plot(self.data[np.argmin(v), :], color='blue', linestyle='-', lw=4, label='min')
                ax.plot(self.data[np.argmax(v), :], color='red', linestyle='-', lw=4, label='max')
                ax.legend(loc='best')
                ax.set_title('Trajectories at extremes of $\phi_%d$' % self.significantIndices[i])
        plt.tight_layout()
        
        return fig, ax

    def getScale(self, eps_range=None):
        """
        Determine the boundary of acceptable epsilon scaling values
        for use in the diffusion map function.
    
        Parameters
        ==========
        
        data : (np.ndarray)
            The data.
            Shape (npoints, ndims).
        
        eps_range : (float, optional)
            A range of "characteristic distances" over which to analyze the diffusion map.
            Defaults to 10 ** -6 to 10 ** 6.
            
        Returns
        =======
        
        Plot of the ratio of the sum of the weight matrix versus its maximum value for
        each epsilon value in eps_range.
        """
        fig, ax = DiffusionMap.diffusion_map_scaling(similarityMatrix=self.similarityMatrix, eps_range=eps_range)
        ax.axvline(self.eps, color='red')
        
        return fig, ax
    
    def NystromExtension(self, newSimilarityMatrix):
        """
        This method calls the ComputeNystromExtension method in a loop to compute the Nystrom
        extension to the eigenvectors for multiple data points.
        
        Parameters
        ==========
        
        newSimilarityMatrix : (np.ndarray)
            A partial distance matrix composed of the distance between each of
            the new data points and the original data. A symmetric distance measure
            must be used so that a full distance matrix can be constructed from this
            partial distance matrix. This method assumes that the distance between
            a data point and itself is zero.
            Shape (#old data + #new data, #old data).
            
        Returns
        =======
        
        neweigenvectors : (np.ndarray)
            An array of the m eigenvectors extended to the n+1 points.
            Shape (n+1, m).
        """
        numberOfOldDataPoints = self.similarityMatrix.shape[0]
        numberOfNewDataPoints = newSimilarityMatrix.shape[0] - numberOfOldDataPoints
        neweigenvectors = np.empty((self.eigenvectors.shape[0] + numberOfNewDataPoints, self.eigenvectors.shape[1]))
        neweigenvectors[:self.eigenvectors.shape[0], :] = self.eigenvectors
        distances = np.empty((numberOfOldDataPoints + 1, numberOfOldDataPoints + 1))
        distances[:numberOfOldDataPoints, :numberOfOldDataPoints] = newSimilarityMatrix[:numberOfOldDataPoints, :numberOfOldDataPoints]
        distances[-1, -1] = 0
        for i in range(numberOfNewDataPoints):
            distances[-1, :-1] = newSimilarityMatrix[numberOfOldDataPoints + i, :]
            distances[:-1, -1] = distances[-1, :-1]
            neweigenvectors[numberOfOldDataPoints + i, :] = self._ComputeNystromExtension(distances)[None, :]
        
        return neweigenvectors
    
    def _ComputeNystromExtension(self, newSimilarityMatrix):
        """
        The Nystrom extension is used to extend eigenvectors to additional data points through an 
        approximation method using the diffusion kernel.
        
        This method will compute the Nystrom extension for one additional data point.
        
        Parameters
        ==========
        
        newSimilarityMatrix : (np.ndarray)
            A symmetric distance matrix of the original n data points along with
            the one additional data point.
            Shape (n+1, n+1).
            
        Returns
        =======
        
        neweigenvectors : (np.ndarray)
            An array of the m eigenvectors extended to the n+1 points.
            Shape (n+1, m).
        """
        # Apply the kernel.
        weights = np.exp(-self.similarityMatrix ** 2 / self.eps ** 2)
        newweights = np.exp(-newSimilarityMatrix ** 2 / self.eps ** 2)
    
        # Sum over the columns of the weight matrix to form the diagonal normalization matrix.
        assert np.all(np.sum(weights, axis=1) != 0)
        normalization = np.diag(np.sum(weights, axis=1) ** (-self.alpha))
        
        # Normalize the weight matrix with the normalization matrix.
        normedweights = np.dot(np.dot(normalization, weights), normalization)
    
        # Compute the normalization for the new data point.
        assert np.all(np.sum(newweights, axis=1) != 0)
        newnormalization = np.sum(newweights, axis=1) ** (-self.alpha)
        
        # Normalize the weight matrix with the normalization matrix.
        newnormedweights = np.zeros((normedweights.shape[0] + 1, normedweights.shape[1] + 1))
        newnormedweights[:-1, :-1] = normedweights
        newnormedweights[-1, :] = newweights[-1, :] * (newnormalization * newnormalization[-1])
        newnormedweights[:, -1] = newnormedweights[-1, :]

        # Form the diffusion matrix.
        assert np.all(np.sum(normedweights, axis=1) != 0)
        newnormedweights[:-1, :-1] /= np.sum(normedweights, axis=1)
        
        assert np.all(np.sum(newnormedweights, axis=1) != 0)
        newnormedweights[-1, :] /= np.sum(newnormedweights[-1, :])
        newnormedweights[:-1, -1] /= np.sum(newnormedweights[:-1, :], axis=1)
        diffusionmatrix = newnormedweights
        
        # Compute the new eigenvector entries.
        neweigenvectors = np.empty((self.eigenvectors.shape[1]))
        for i in range(self.eigenvectors.shape[1]):
            neweigenvectors[i] = (1 / self.eigenvalues[i]) * np.sum(diffusionmatrix[-1, :-1] * self.eigenvectors[:, i])

        return neweigenvectors

    @staticmethod
    def diffusion_map(similarityMatrix, alpha=1, eps=None, k=10):
        """
        Return the eigenvalues and eigenvectors of a diffusion map on either
        data or a distance kernel matrix.
    
        Parameters
        ==========
        
        similarityMatrix : (np.ndarray)
            The similarity matrix to be analyzed by diffusion maps.
            Shape (nPoints, nPoints).
        
        alpha : (int, optional)
            The type of diffusion. Typical choices include 0, 1/2, or 1.
        
        eps : (float, optional)
            The "characteristic distance" in the dataset.
            Defaults to the median of the pairwise distances.
        
        k : (int, optional)
            Number of eigenvectors and corresponding eigenvalues to compute.
        
        Returns
        =======
        
        eigenvalues : (np.ndarary)
            The importance of the embedding dimenensions, but watch out for harmonics.
            Shape (npoints,).
        
        eigenvectors : (np.ndarray)
            The new embeddings of the data points.
             Shape (npoints, npoints).
        
        diffusionMatrix : (np.ndarray)
            The diffusion matrix for which the eigenvectors and eigenvalues are calculated.
             Shape (npoints, npoints).
        """    
        # Calculate an epsilon.
        if eps is None:
            eps = np.median(similarityMatrix[np.triu_indices(similarityMatrix.shape[0], 1)])
        assert(eps != 0)
        
        # Apply the kernel.
        weights = np.exp(-similarityMatrix ** 2 / eps ** 2)
        
        # Sum over the columns of the weight matrix to form the diagonal normalization matrix.
        normalization = np.diag(np.sum(weights, axis=1) ** (-alpha))
        
        # Normalize the weight matrix with the normalization matrix.
        normedweights = np.dot(np.dot(normalization, weights), normalization)
        
        # Sum over the columns of the normalized weight matrix to form a new diagonal matrix.
        assert np.all(np.sum(normedweights, axis=1) != 0)
        newdiagonal = np.diag(np.sum(normedweights, axis=1) ** (-1))
        
        # Form the diffusion matrix.
        diffusionmatrix = np.dot(newdiagonal, normedweights)
        
        # Form a similarity transformation matrix.
        similaritytransform = np.diag(np.sum(normedweights, axis=1) ** (-1/2))
        
        # Form a similar matrix to the diffusion matrix.
        similar = np.dot(np.dot(similaritytransform, normedweights), similaritytransform)
        
        # Subtract off the skew-symmetric part of S.
        # S is symmetric by construction, this is to ensure symmetry.
        assert np.allclose((similar - similar.T), 0)
        similar = similar - 0.5 * (similar - similar.transpose())
        
        assert np.allclose(similar - similar.T, 0)
        
        # Calculate the eigenvalues and eigenvectors of S.
        eigenvalues, eigenvectors = eigsh(similar, k=k)
    
        # Check for any imaginary part generated by the eigensolver.
        assert (eigenvalues.imag == 0).all()
        assert (eigenvectors.imag == 0).all()
    
        # Transform the eigenvectors of S to eigenvectors of the original diffusion matrix.
        eigenvectors = np.dot(similaritytransform, eigenvectors)
        for i in range(k):
            eigenvectors[:, i] = eigenvectors[:, i] / np.sqrt(np.sum(eigenvectors[:, i] ** 2, axis=0))
        
        order = np.argsort(eigenvalues)[::-1]
        eigenvalues = eigenvalues[order]
        eigenvectors = eigenvectors[:, order]
        
        return (eigenvalues.ravel(), eigenvectors, eps, diffusionmatrix)

    @staticmethod 
    def diffusion_map_scaling(similarityMatrix, eps_range=None):
        """Determine the boundary of acceptable epsilon scaling values
        for use in the diffusion map function.
    
        Parameters
        ==========
        
        similarityMatrix : (np.ndarray)
            The distance matrix to analyze.
             Shape (npoints, ndims).
        
        eps_range : (np.ndarray, optional)
            A range of "characteristic distances" over which to analyze the diffusion map.
            Defaults to 200 log spaced values between 10 ** -6 and 10 ** 6.
        
        Returns
        =======
        
        Plot of the ratio of the sum of the weight matrix versus its maximum value for
        each epsilon value in eps_range.
        """
        # Choose a default eps range if none is given. 
        if eps_range is None:
            eps_range = np.logspace(-6, 6, num=200)
        
        ratio = np.empty(len(eps_range))
        
        for i, eps in enumerate(eps_range):
            # Apply the kernel.
            weights = np.exp(-similarityMatrix ** 2 / eps ** 2)
        
            # Calculate the sum ratio.
            ratio[i] = (np.sum(weights) / weights.size)
            
        fig, ax = plt.subplots()
        ax.semilogx(eps_range, ratio)
        ax.set_xlabel('Epsilon values')
        ax.set_ylabel('W/$W_{max}$')
        
        return (fig, ax)

    @staticmethod
    def local_linear_regression(x, y, basis_point_index, eps_scale=3, bandwidth=None, bandwidth_type='median'):
        """Return the local linear fit to the measured data, y, at the basis point indicated.
        
        Parameters
        ==========
        x : (np.ndarray)
           The input points of the regression. 
           Shape (nPoints, mDimensions).
        
        y : (np.ndarray)
            The measured value of the observable at each input point.
             Shape (nPoints).
        
        basis_point_index : (int)
            The row index of the data point used as the origin of the regression.
        
        eps_scale : (float, optional)
            A scaling value for the closeness metric used in the bandwidth calculation.
        
        bandwidth: (float, optional)
            Allows for the bandwidth value to be specified, instead of calculated for each regression.
            Used to feed in a bandwidth value for each collection of eigenvectors, as the Euclidean metric
            chosen for the bandwidth calculation is translation invariant. (Doesn't depend on the basis point)
        
        bandwidth_type : (median or mean, optional)
            The type of metric used for determining closeness in the bandwidth calculation.        
    
        Returns
        =======
        
        y_fit : (np.ndarray)
            The local linear fit to the measured data, y, at the specified basis point.
             Shape (nPoints).
        
        """
        # Calculate a bandwidth.
        if bandwidth is None:
            if bandwidth_type == 'median':
                bandwidth = np.power((np.median(squareform(pdist(x))) / eps_scale), 2)
            elif bandwidth_type == 'mean':
                bandwidth = np.power((np.mean(squareform(pdist(x))) / eps_scale), 2)
            else:
                raise ValueError('bandwidth_type must be either median or mean')
        else:
            bandwidth = bandwidth
        if bandwidth == 0:
            bandwidth = np.finfo(float).eps
    
        # Perform the local linear regression for the given basis point index.
        basis_point_index = int(basis_point_index)
        x0 = x - x[basis_point_index, :]
        weights = np.diag(np.exp(-np.power(np.linalg.norm(x0, axis=1), 2) / bandwidth))
        xx = np.concatenate((np.ones((x0.shape[0], 1)), x0), axis=1)
        betas = np.linalg.lstsq(
                    np.dot(xx.transpose(), np.dot(weights, xx)), 
                    np.dot(xx.transpose(), np.dot(weights, y))
                    , rcond=None)[0]
        y_fit = np.dot(xx, betas)
        
        return y_fit

    @staticmethod
    def compute_residuals(eigenvectors, eps_scale=3, skipFirst=True, bandwidth_type='median'):
        """Return the residuals for a local linear regression on
        the eigenvectors of a diffusion map.
        
        Parameters
        ==========
        
        eigenvectors : (np.ndarray)
            The eigenvectors to be fit. Should include all of the eigenvectors resulting from the
            diffusion map calculation, including the first, trivial eigenvector.
             Shape(npoints, nvectors).
        
        eps_scale : (float, optional)
            A scaling value for the closeness metric used in the bandwidth calculation. 
        
        skipFirst : (bool, optional)
            True - define resisual[0] as 0 and residual[1] as 1.
            False - residual[0] is 1 and the others are all calculated.
        
        bandwidth_type : (median or mean, optional)
            The type of metric used for determining closeness in the bandwidth calculation.      
    
        Returns
        =======
        
        result : (dict) - Contains the eps scale, bandwidth type and residuals resulting from the calculation.
        
        residuals : (np.ndarray)
            A measure of the degree of the goodness of the fit, lower values indicate a better fit.
            The first residual for diffusion maps should be ignored and the second is set to one by definition.
             Shape(nresiduals).
        """
        # Check for more than two eigenvectors.
        assert eigenvectors.shape[1] > 2, 'There must be more than two eigenvectors to compute residuals.'
    
        # Set up the residuals and define the second as one.
        residual = np.zeros(eigenvectors.shape[1])
        if skipFirst:
            residual[1] = 1
            firstIndex = 2
        else:
            residual[0] = 1
            firstIndex = 1
    
        ivals = range(firstIndex, eigenvectors.shape[1])
        jvals = np.arange(eigenvectors.shape[0])
                
        print('Calculating {0} residuals'.format(len(residual) - 2))
        pbar = tqdm(total=len(ivals), desc='Calculating {0} residuals'.format(len(residual) - 2))

        # Run the residual calculation for each eigenvector.    
        for i in ivals:
            num, den = 0, 0
    
            if bandwidth_type == 'median':
                bandwidth = np.power((np.median(squareform(pdist(eigenvectors[:,:i]))) / eps_scale), 2)
            elif bandwidth_type == 'mean':
                bandwidth = np.power((np.mean(squareform(pdist(eigenvectors[:,:i]))) / eps_scale), 2)
            else:
                raise ValueError('bandwidth_type must be either median or mean')
    
            if bandwidth == 0:
                bandwidth = np.finfo(float).eps
            
            regression = partial(DiffusionMap.local_linear_regression, eigenvectors[:, :i], eigenvectors[:, i], bandwidth=bandwidth, bandwidth_type=bandwidth_type)
    
            fit = np.array(list(tqdm(map(regression, jvals), total=len(jvals), leave=False)))[jvals, jvals]
            num = np.sum(np.power(eigenvectors[:, i] - fit, 2))
            den = np.sum(np.power(eigenvectors[:, i], 2))
                
            # Compute a residual for eigenvector i.
            residual[i] = np.sqrt(num / den)
            
            pbar.update()
        result = dict([('Residuals', residual), ('Eps Scale', eps_scale), ('Bandwidth Type', bandwidth_type)])
        
        return result

    @staticmethod
    def residualsPerEps(eigenvectors, eps_scale_range=[0.1, 5, 10], bandwidth_type='median'):

        residualsPerEps = {}
        for eps in tqdm(sorted(eps_scale_range), desc='eps_med_scale'):
            residualsPerEps[eps] = DiffusionMap.compute_residuals(eigenvectors=eigenvectors, eps_scale=eps, bandwidth_type=bandwidth_type)['Residuals']
        fig, ax = plt.subplots()
        for eps in residualsPerEps:
            residuals = residualsPerEps[eps]
            ax.plot(range(len(residuals)), residuals, 
            label='$\epsilon = m / %.2f$' % eps,
            marker='o',
            alpha=.5,
            lw=2,
            )
        ax.set_ylabel('residual $r_i$')
        ax.set_xlabel('eigenvector index $i$')
        ax.legend(loc='best')
        ax.set_title('Residuals Per Eps')
        
        return (residualsPerEps, fig, ax)

    @staticmethod
    def scalarsToColormap(scalars, colormap='viridis'):
        """Evaluate a colormap at specified points.
        
        Parameters
        ==========
        
        scalars : (iterable of numeric)
        
        colormap : (string, optional)
            Something from `matplotlib.cm` (e.g. "hot" or "jet").
            
        Returns
        =======
        
        colors : (np.ndarray)
            The colors as (r, g, b, alpha) tuples.
             Shape (len(scalars), 4).
        
        cmap : (a ScalarMappable instance)
        """
        import matplotlib as mpl    
        cmap = mpl.cm.ScalarMappable(norm=None, cmap=colormap)    
        cmap.set_array(scalars)
        colors = cmap.to_rgba(scalars)
        return (colors, cmap)

if __name__ == '__main__':
    # Test the diffusion map code on a circle.
    circleData = np.array([[np.cos(t), np.sin(t)] for t in np.linspace(0, 2 * np.pi)])
    k = 4
    dmapTest = DiffusionMap(circleData, k=k)
    dmapTest.gridPlot()
    dmapTest.LLRplot()
    dmapTest.getScale()
    dmapTest.status
    
    # Check the eigenvector computations of the transformed diffusion matrix.
    eigvals, eigvecs = sp.sparse.linalg.eigs(dmapTest.diffusionMatrix, k=4)
    order = np.argsort(eigvals)[::-1]
    diffusionMatrixEigenvalues = eigvals[order]
    similarMatrixEigenvalues = dmapTest.eigenvalues
    diffusionMatrixEigenvectors = eigvecs[:, order]
    similarMatrixEigenvectors = dmapTest.eigenvectors
    eigenvalueDifference = diffusionMatrixEigenvalues - similarMatrixEigenvalues
    eigenvectorDifference = np.absolute(diffusionMatrixEigenvectors) - np.absolute(similarMatrixEigenvectors)
    print(np.amax(eigenvalueDifference), np.amax(eigenvectorDifference))
    
    # Test the Nystrom extension method.
    coarseData = np.array([[np.cos(t), np.sin(t)] for t in np.linspace(0, 2 * np.pi, 20)])
    nystromDmap = DiffusionMap(coarseData, k=k)
    
    fineData = np.array([[np.cos(t), np.sin(t)] for t in np.linspace(0, 2 * np.pi, 100)])
    data = np.vstack((coarseData, fineData))
    distances = np.empty((data.shape[0], coarseData.shape[0]))
    distances[:coarseData.shape[0], :coarseData.shape[0]] = squareform(pdist(coarseData))
    for i in range(fineData.shape[0]):
        distances[coarseData.shape[0] + i, :] = squareform(pdist(np.vstack((coarseData, fineData[i, :]))))[-1, :-1]
    
    neweigenvectors = nystromDmap.NystromExtension(distances)
    fig, ax = plt.subplots(nrows=k, ncols=1, figsize=(4, 16))
    for i in range(k):
        ax[i].scatter(np.linspace(0, 2 * np.pi, coarseData.shape[0]), nystromDmap.eigenvectors[:, i], s=1);
        ax[i].scatter(np.linspace(0, 2 * np.pi, fineData.shape[0]), neweigenvectors[coarseData.shape[0]:, i], s=1);
        ax[i].set_xlabel('Time'); ax[i].set_ylabel(r'$\phi_{0}$'.format(i));
        ax[i].legend(['True', 'Nystrom Approximation']);
