U = linspace(0, 2, 100);
V = linspace(0, 2, 100);

[uu, vv] = meshgrid(U, V);

func = exp(-0.5*uu.*vv);

figure()
contourf(uu,vv,func,13)
xlabel('\boldmath$x$', 'Interpreter', 'Latex', 'FontSize', 12);
ylabel('\boldmath$y$', 'Interpreter', 'Latex', 'FontSize', 12);
title('$f(x,y) = e^{-\frac{1}{2}x \cdot y}$', 'Interpreter', 'Latex', 'FontSize', 12);