function [xy_lines, x2y2_lines] = wire_grid(xy_vals, x2y2_vals, N)
    if ~exist('N', 'var') || isempty(N)
        N = 250;
    end
    
    xy_vals = xy_vals(:);
    x2y2_vals = x2y2_vals(:);
    
    U = linspace(min(xy_vals), max(xy_vals), N);
    V = linspace(min(x2y2_vals), max(x2y2_vals), N);
    
    xy_lines = zeros(length(xy_vals), 2, N);
    x2y2_lines = zeros(length(x2y2_vals), 2, N);
    
    for i = 1:length(xy_vals)
        [X, Y] = xy_from_uv(xy_vals(i), V);
        xy_lines(i,1,:) = X;
        xy_lines(i,2,:) = Y;
    end
    
    for i = 1:length(x2y2_vals)
        [X, Y] = xy_from_uv(U, x2y2_vals(i));
        x2y2_lines(i,1,:) = X;
        x2y2_lines(i,2,:) = Y;
    end
end
function [X, Y] = xy_from_uv(U, V)
%XY_FROM_UV computes (x, y) in Q1 from (u, v) = (xy, x^2 - y^2).
%   [Detailed explanation goes here.]
    Y = sqrt((-V + sqrt(V.^2 + 4*U.^2))/2);
    X = U./Y;
end