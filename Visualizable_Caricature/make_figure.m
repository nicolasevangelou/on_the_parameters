large = 6; % 8
small = 4; % 6
thick = 2.5; % 3
thin = 1.5; % 2

N = 500;
U = linspace(0.25, 2, 8);
V = linspace(-3, 3, 7);
[uu, vv] = wire_grid(U, V, N);

xy_fm = cell(1, size(U, 2));
xy_fm(:) = {'r-'};
xy_fm{3} = 'g-';

x2y2_fm = cell(1, size(V, 2));
x2y2_fm(:) = {'r-'};
x2y2_fm{3} = 'b-';

func = @(z) exp(-0.5*z);
fig = plot_wires_3d(uu, vv, func, xy_fm, x2y2_fm);
% 'k-',  exp(-0.375));

for i = 1:length(U)
    if ~strcmp('r-', xy_fm{i})
        xx = reshape(uu(i,1,:), [N, 1]);
        yy = reshape(uu(i,2,:), [N, 1]);
        plot3(xx, yy, zeros(size(xx)), xy_fm{i}, 'LineWidth', thick);       
        x = xx(end-40); y = yy(end-40);
        plot3([x, x], [y, y], [0, func(x*y)], 'k:','LineWidth', 1.5);
        plot3([x, x], [y, y], [0, func(x*y)], 'ko', 'MarkerSize', ...
              small, 'LineWidth', 1,'MarkerFaceColor','k');
    end
end

Ns = 500;
pts = [0.5*rand(Ns, 2), zeros(Ns, 1)] + [0.5, 0.9, 0.0];
pts(:,3) = func(pts(:,1).*pts(:,2));
plot3(pts(:,1), pts(:,2), pts(:,3), 'k.');
plot3(pts(:,1), pts(:,2), zeros(Ns, 1), 'k.');

for i = 1:length(V)
    if ~strcmp('r-', x2y2_fm{i})
        xx = reshape(vv(i,1,:), [N, 1]);
        yy = reshape(vv(i,2,:), [N, 1]);
        plot3(xx, yy, zeros(size(xx)), x2y2_fm{i}, 'LineWidth', thick);
        x = xx(end-30); y = yy(end-30);
        plot3([x, x], [y, y], [0, func(x*y)], 'k:','LineWidth', 1.5);
        plot3([x, x], [y, y], [0, func(x*y)], 'ko', 'MarkerSize', ...
               small, 'LineWidth', 1,'MarkerFaceColor','k');
    end
end
xticks(linspace(0, 2, 5));
yticks(linspace(0, 2, 5));


% alpha_values = linspace(0,0.85,100);
% for i=1:length(alpha_values)
%    alpha(alpha_values(i))
%    filename = sprintf('f_%03d.png', i-1);
%    saveas(fig, filename);
% end

% figure()
% hold on;
% Data = [];
% for i =1:8
%    xi = reshape(uu(i,1,:),500,1);
%    yi = reshape(uu(i,2,:),500,1);
%    Data = [Data ;xi yi]
%    plot(xi,yi,'k.')  
%     
% end


% for i =1:7
%    xi = reshape(vv(i,1,:),500,1);
%    yi = reshape(vv(i,2,:),500,1);
%    plot(xi,yi,'k.')  
%    Data = [Data ;xi yi]
% 
% end

ax = gca;
ax.XLabel.Position = [0.80, -0.22, -0.13];
set(fig, 'Renderer', 'painters', 'units', 'centimeters'); 
set(fig, 'PaperSize', [9, 6]/2.54, 'position', [5, 5, 9, 6]);
print(fig, 'pnas_fig1.pdf', '-dpdf', '-r300');