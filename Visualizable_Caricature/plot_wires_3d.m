function fig = plot_wires_3d(xy_lines, x2y2_lines, func, xy_fmt, x2y2_fmt, base_fmt, cut)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 7
        cut = -1;
        if nargin < 6
            base_fmt = 'k-';
            if nargin < 5
                x2y2_fmt = 'r-';
                if nargin < 4
                    xy_fmt = 'r-';
                    if nargin < 3
                        func = @(z) exp(-0.5*z);
                    end
                end
            end
        end
    end
    
    thick = 2; % 3
    thin = 1; % 2
    
    fig = figure(); view(75, 25); hold('on'); grid('on'); box('on');
    xlabel('\boldmath$p_1$', 'Interpreter', 'Latex', 'FontSize', 12);
    ylabel('\boldmath$p_2$', 'Interpreter', 'Latex', 'FontSize', 12);
    zlabel('\boldmath$f(p_1,p_2)$', 'Interpreter', 'Latex', 'FontSize', 12);
    xtickformat('%.1f'); ytickformat('%.1f'); ztickformat('%.1f');
    
    if cut >= 0
        xmin = min(min(xy_lines(:,1,:), [], 'all'), ...
                   min(x2y2_lines(:,1,:), [], 'all'));
        xmax = max(max(xy_lines(:,1,:), [], 'all'), ...
                   max(x2y2_lines(:,1,:), [], 'all'));
        ymin = min(min(xy_lines(:,2,:), [], 'all'), ...
                   min(x2y2_lines(:,2,:), [], 'all'));
        ymax = max(min(xy_lines(:,2,:), [], 'all'), ...
                   max(x2y2_lines(:,2,:), [], 'all'));
        
        surf([xmin, xmax], [ymin, ymax], cut*ones(2, 2));
        colormap(gray); alpha(0.4);
    end
    
    N = size(xy_lines, 3);
    for i = 1:size(xy_lines, 1)
        if ischar(xy_fmt)
            fmt = xy_fmt;
        else
            fmt = xy_fmt{i};
        end
        if strcmp(fmt, 'r-')
            width = thin;
        else
            width = thick;
        end
        
        xx = reshape(xy_lines(i,1,:), [N, 1]);
        yy = reshape(xy_lines(i,2,:), [N, 1]);
        if width == thin
            plot3(xx, yy, zeros(size(xx)), base_fmt);
        end
        plot3(xx, yy, func(xx.*yy), fmt, 'LineWidth', width);
    end
    
    N = size(x2y2_lines, 3);
    for i = 1:size(x2y2_lines, 1)
        if ischar(xy_fmt)
            fmt = x2y2_fmt;
        else
            fmt = x2y2_fmt{i};
        end
        if strcmp(fmt, 'r-')
            width = thin;
        else
            width = thick;
        end
        
        xx = reshape(x2y2_lines(i,1,:), [N, 1]);
        yy = reshape(x2y2_lines(i,2,:), [N, 1]);
        if width == thin
            plot3(xx, yy, zeros(size(xx)), base_fmt);
        end
        plot3(xx, yy, func(xx.*yy), fmt, 'LineWidth', width);
    end
end